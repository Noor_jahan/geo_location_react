import {React,useEffect,useState,useCallback} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import login_Logo from './assets/images/loginScreen.png'
import { BrowserRouter as Router} from 'react-router-dom'
import { browserHistory, Route } from 'react-router';
import Button from '@material-ui/core/Button';
import UseGeoLocation from './Components/UseGeoLocation';
import LandingPage from './Components/LandingPage';
import UserInfo from './Components/UserInfo';

export default function App() {
  // console.log(localStorage.getItem('login'));
  const [vehicleNumber,setVehicleNumber] = useState("");
  const [taxiCompany,setTaxiCompany] = useState("");
  const [name,setName] = useState("");
  const [mobile,setMobileNumber] = useState("");
  let stateValues = {vehicleNumber,taxiCompany,name,mobile}
  let actions = {
    setVehicleNumber:(data)=>{setVehicleNumber(data)},
    setTaxiCompany:(data)=>{setTaxiCompany(data)},
    setName:(data)=>{setName(data)},
    setMobileNumber:(data)=>{setMobileNumber(data)}
  }
  return (
    <Router>
      {/* <Route exact path="/" component={UserInfo}/> */}
      {/* <Route path="/location" component={LandingPage}/> */}
      <Route exact path="/" ><UserInfo actions={actions} stateValues={stateValues}/></Route>
      <Route path="/location"><LandingPage actions={actions} stateValues={stateValues}/></Route>
    </Router>
  );
}
