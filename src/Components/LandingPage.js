
import {React,useEffect,useState,useCallback} from 'react';
import axios from 'axios';
import UseGeoLocation from './UseGeoLocation';

export default function LandingPage(props){
  console.log('name   ',props?.stateValues?.name, '  taxiCompany ',props?.stateValues?.taxiCompany,'  vehicleNumber ',props?.stateValues?.vehicleNumber,' mobile  ',props?.stateValues?.mobile)

    const[apiData,setApiData] = useState();
    useEffect(()=>{
      axios.get("/login").then(response=>{setApiData(response?.data?.message)})
    },[])
    const [ currentPosition, setCurrentPosition ] = useState();
    const [apiResponse,setApiResponse] = useState();
    const [postId, setPostId] = useState(null);
  
    const success = position => {
      // console.log('position.coords.latitude ',position)
      
      const currentPosition = {
        lat: ((position.coords.latitude)),
        lng: ((position.coords.longitude))
      }
      setCurrentPosition(currentPosition);
    };
    
    useEffect(() => { 
      let isMounted = true
      const intervalId = setInterval(()=>{
          navigator.geolocation.getCurrentPosition(success);
        },2000);
        return () => {
          clearInterval(intervalId); //This is important
          isMounted = false // Let's us know the component is no longer mounted.
      }
      },[currentPosition])
      useEffect(()=>{navigator.geolocation.getCurrentPosition(success);},[])
  
      useEffect(()=>{postCallApi()},[currentPosition])
  
      useEffect(()=>{callApi();})
  
      const callApi = () => {
        fetch("http://localhost:9000/location")
        .then(res => res.text())
        .then(res => {
          console.log('apiResponse,',res)
          setApiResponse(res)
        })
      }
      const postCallApi = () => {
        // POST request using fetch inside useEffect React hook;
        let Obj = { name:props?.stateValues?.name, taxiCompany:props?.stateValues?.taxiCompany,vehicleNumber:props?.stateValues?.vehicleNumber,mobile:'+'+props?.stateValues?.mobile,latitude:currentPosition?.lat,longitude:currentPosition?.lng}
          const requestOptions = {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(Obj)
          };
          fetch('http://localhost:9000/handle', requestOptions)
              .then(res => res.text())
              .then(res => { setPostId(JSON.stringify(res))})
      }
    return(
      <div className="App">
        <header className="App-header">
          <UseGeoLocation currentPosition={currentPosition} postId={postId} apiResponse={apiResponse}/>
        </header>
      </div>
    )

}
