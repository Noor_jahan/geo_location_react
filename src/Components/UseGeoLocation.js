import {React,useEffect,useState,useCallback} from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import {Icon} from 'leaflet'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { CSVLink } from "react-csv";
import NoSleep from 'nosleep.js';
var noSleep = new NoSleep();

export default function UseGeoLocation(props){
      useEffect(()=>{
        noSleep.enable();
        return function cleanUp(){noSleep.disable();}
      },[props?.currentPosition])
      // console.log('apiResponse,',props)
      const headers = [
        { label: "Vehicle Number", key: "vehicle_Number" },
        { label: "Taxi Company", key: "taxi_Company" },
        { label: "Name", key: "name" },
        { label: "Mobile Number", key: "mobile_Number" },
        { label: "Latitude", key: "latitude" },
        { label: "Longitude", key: "longitude" },
        { label: "Updated Time", key: "updated_Time" }
      ];
      const data = props?.apiResponse
     return(
        <>
         {props?.currentPosition?.lat && <Snackbar open={true} autoHideDuration={6000}>
          <Alert  severity="success">
            {props?.postId}
          </Alert>
          
        </Snackbar>}
        {/* <CSVLink
            data={props?.apiResponse} headers={headers}
            filename={"my-file.csv"}
            className="btn btn-primary"
            target="_blank"
          >
            Click here to download report
          </CSVLink> */}
        {props?.currentPosition?.lat && <MapContainer center={[props?.currentPosition?.lat, props?.currentPosition?.lng]} zoom={13} style={{ height: '100vh', width: '100wh' }}>
         <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={[props?.currentPosition?.lat, props?.currentPosition?.lng]} icon={new Icon({iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41]})} >
          <Popup style={{top:"-105px"}}>
            <span>A pretty CSS3 popup. <br/> Easily customizable.</span>
          </Popup>
        </Marker>
      </MapContainer>}</>
     )
 }


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}