import {React,useState,useEffect} from 'react';
import { Card } from '@material-ui/core';
import loginImg from  '../assets/images/bluGraph_Logo.png';
import phoneUtil from 'google-libphonenumber'
import TextField from '@material-ui/core/TextField';
import { useHistory } from 'react-router-dom';

export default function UserInfo(props) {
    localStorage.setItem('login',false)
    const history = useHistory()
    const phoneUtil1 = phoneUtil.PhoneNumberUtil.getInstance();
    const submitDetails = () =>{
        if(props?.stateValues?.name !== "" && props?.stateValues?.taxiCompany !== "" && props?.stateValues?.vehicleNumber !== "" && props?.stateValues?.mobile !== ""){
        //     const numberValue = phoneUtil.PhoneNumberUtil.getInstance();
        //     const phone = numberValue.parse(mobile);
            // console.log('numberValue',numberValue.isValidNumber(phone))
            // if(!numberValue.isValidNumber(phone)){
            //     alert('Please Enter a valid Phone number with Country code');
            //     return 0
            // }
            localStorage.setItem('login',true);
            history.push('/location')
        }else if(props?.stateValues?.name == "" || props?.stateValues?.taxiCompany == "" || props?.stateValues?.vehicleNumber == "" || props?.stateValues?.mobile == ""){
            alert('Please fill required fields')
        }
    }
    

  return (
    <div className="container" style={{display: "contents"}}>
        <div className ="card" style={{background: "#3b3bf3",height: "100px",width: "auto"}}>
            <img src={loginImg} width={90}/>
            <h6 style={{ fontSize: "20px",textAlign: "center", marginTop: "-3.5rem"}}> Blu Graph</h6>
        </div>
        <div className="card">
            <div className="card-body">
                <form>
                    <TextField id="Vehicle Number"  required label="Vehicle Number" value={props?.stateValues?.vehicleNumber} onChange={(e)=>{props?.actions?.setVehicleNumber(e.target.value)}} style={{width: "320px",marginBottom: "15px"}} />
                    <TextField id="Taxi Company"    required label="Taxi Company"   value={props?.stateValues?.taxiCompany}   onChange={(e)=>{props?.actions?.setTaxiCompany(e.target.value)}} style={{width: "320px",marginBottom: "15px"}} />
                    <TextField id="Mobile Number"   required type="number"  label="Mobile Number"  value={props?.stateValues?.mobile}        onChange={(e)=>{props?.actions?.setMobileNumber(e.target.value)}} style={{width: "320px",marginBottom: "15px"}} className={'validNumberClass'}/>
                    <TextField id="Name"            required label="Name"           value={props?.stateValues?.name}          onChange={(e)=>{props?.actions?.setName(e.target.value)}} style={{width: "320px",marginBottom: "15px"}}/>
                    <button type="button" className="button buttonBlue" onClick={()=>submitDetails()}>Submit
                        <div className="ripples buttonRipples"><span className="ripplesCircle"></span></div>
                    </button>
                </form>
            </div>
        </div>
    </div>
  );
}